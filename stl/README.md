Organizer in Modular Grid
=========================
SD Cards
--------
- [2x1 SD Card - space for 3 cards](sdcard-2x1.stl)
- [2x2 SD Card - space for 6 cards](sdcard-2x2.stl)
- [2x3 SD Card - space for 12 cards](sdcard-2x3.stl)

Micro SD Cards
--------------
- [1x1 Micro SD Card - space for 7 cards](microsdcard-1x1.stl)
- [1x2 Micro SD Card - space for 17 cards](microsdcard-1x2.stl)
- [1x2 Micro SD Card - space for 21 cards](microsdcard-2x1.stl)

USB sticks
----------
- [2x2 USB](usb-2x2.stl)

Batteries (AA and AAA)
----------------------
- [2x1 AAA battery, double height, space for 3 AAA batteries](bat-aaa-2x1.stl)
- [2x2 AAA battery, double height, space for 9 AAA batteries](bat-aaa-2x2.stl)
- [2x2 AA battery, double height, space for 4 AAA batteries](bat-aa-2x2.stl)
- [3x2 AA battery, double height, space for 8 AAA batteries](bat-aa-3x2.stl)
- [2x3 AA battery, double height, space for 8 AAA batteries](bat-aa-2x3.stl)


Screw bits (hex)
----------------
- [1x1 screwbits - space for 4 bits](screwbits-1x1.stl)
- [2x1 screwbits - space for 10 bits](screwbits-2x1.stl)
- [1x2 screwbits - space for 10 bits](screwbits-2x1.stl)
- [2x2 screwbits - space for 25 bits](screwbits-2x2.stl)

Generic trays
-------------
- [1x1 single height, with single tray](tray-1x1x1-single.stl)
- [2x2 single height, with single tray](tray-2x2x1-single.stl)
- [3x2 single height, with 4 trays](tray-3x2x1-4-trays.stl)
- [3x1 double height, with 2 trays](tray-3x1x2-2-trays.stl)
- [3x2 double height, with 6 trays](tray-3x21x2-6-trays.stl)

Edges
-----
Use these to finish your tray by adding edges. Use the 'extended' sides to fill in the corners. To finish all edges, you'll need 2 normal (male and female) and 2 extended (male and female) edges. You can choose either rounded or square corners.

**female**
- Edges with no rounding, female [1](edge-f-1-no-rounding.stl), [2](edge-f-2-no-rounding.stl), [3](edge-f-3-no-rounding.stl), [4](edge-f-4-no-rounding.stl)
- Edges with top rounding, female [1](edge-f-1-top-rounding.stl), [2](edge-f-2-top-rounding.stl), [3](edge-f-3-top-rounding.stl), [4](edge-f-4-top-rounding.stl)
- Edges with no rounding, extended sides, female [1](edge-f-1-ext-no-rounding.stl), [2](edge-f-2-ext-no-rounding.stl), [3](edge-f-3-ext-no-rounding.stl), [4](edge-f-4-ext-no-rounding.stl)
- Edges with rounding, extended sides, female [1](edge-f-1-ext-rounded.stl), [2](edge-f-2-ext-rounded.stl), [3](edge-f-3-ext-rounded.stl), [4](edge-f-4-ext-rounded.stl)

**male**
- Edges with no rounding, male [1](edge-m-1-no-rounding.stl), [2](edge-m-2-no-rounding.stl), [3](edge-m-3-no-rounding.stl), [4](edge-m-4-no-rounding.stl)
- Edges with top rounding, male [1](edge-m-1-top-rounding.stl), [2](edge-m-2-top-rounding.stl), [3](edge-m-3-top-rounding.stl), [4](edge-m-4-top-rounding.stl)
- Edges with no rounding, extended sides, male [1](edge-m-1-ext-no-rounding.stl), [2](edge-m-2-ext-no-rounding.stl), [3](edge-m-3-ext-no-rounding.stl), [4](edge-m-4-ext-no-rounding.stl)
- Edges with rounding, extended sides, male [1](edge-m-1-ext-rounded.stl), [2](edge-m-2-ext-rounded.stl), [3](edge-m-3-ext-rounded.stl), [4](edge-m-4-ext-rounded.stl)

Wall-mounts
-----------
Use these to make a wall-mounted tray

- **male** [1](wall-m-1.stl), [2](wall-m-2.stl), [3](wall-m-3.stl), [4](wall-m-4.stl)
- **female** [1](wall-f-1.stl), [2](wall-f-2.stl), [3](wall-f-3.stl), [4](wall-f-4.stl)

