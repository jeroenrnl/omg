include <../src/base.scad>;
include <../src/usb.scad>;
include <../src/tray.scad>;
include <../src/screwbits.scad>;
include <../src/sdcard.scad>;
include <../src/batteries.scad>;

color("grey") {
    translate([0,0,0]) {
        usb(2,2,true,2);
    }
    translate([LENGTH*2,0,20]) {
        tray(3,2,1,2,2);
    }
    translate([0,WIDTH*2,40]) {
        screwbits(3,1,true);
    }

    translate([LENGTH*3,WIDTH*2,60]) {
        sdcard(3,1, true);
    }

    translate([LENGTH*5,0,40]) {
        microsdcard(1,2,true);

    }

    translate([0,WIDTH*3,55]) {
        aaa(2,1,2,true);
    }
    translate([LENGTH*2,WIDTH*3,85]) {
        aa(3,1,2,true);
    }
    translate([LENGTH*5,WIDTH*3,115]) {
        tray(1,1,2,1,1);
    }

    translate([0,-WIDTH,5]) {
        mirror([0,1,0]) {
            edge_m(6,false,false, true);
        }
    }
    translate([-LENGTH, 0, 0]) {
        rotate([0,0,90]) {
            edge_m(4,true,true, true, true);
        }
    }
    translate([0,WIDTH*5,30]) {
        edge_f(6,false,false,true);
    }
    translate([LENGTH*7,WIDTH*4,35]) {
        rotate([0,0,-90]) {
            edge_f(4,true,true,true,true);
        }
    }


}