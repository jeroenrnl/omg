Organizer in Modular Grid
=========================

This shows you an example of how a finished product could look using various modules. Do not use this to print, as you will not be able to take the tray apart.

![demo](demo.png "rendering of a complete desk tray")

![exploded view](demo-exploded.png "exploded view of the demo")

![wallmount](demo-wallmount.png "demo of wallmounted tray")
