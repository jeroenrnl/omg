include <../src/base.scad>;
include <../src/usb.scad>;
include <../src/tray.scad>;
include <../src/screwbits.scad>;
include <../src/sdcard.scad>;
include <../src/batteries.scad>;

color("grey") {
    usb(1,2,true,2);
    translate([LENGTH*2,0,0]) {
        tray(2,2,1,1,2);
    }
    translate([LENGTH,0,0]) {
        screwbits(1,2,true);
    }

    translate([0,WIDTH*2,0]) {
        sdcard(5,1, true);
    }

    translate([LENGTH*4,0,0]) {
        microsdcard(1,2,true);

    }


    wall_m(3);

    mirror([0,1,0]) {
        edge_m(5,true,true, true, false);
    }
    translate([LENGTH*5,0,0]) {
        wall_f(3);
    }
    translate([0,WIDTH*3,0]) {

            edge_f(5);
    }


}