include <base.scad>;

module aaa(x,y,z, CENTER=false, SPACING=2) {

    SIZEX = 11;
    SIZEY = 11;
    SIZEZ = 10 + (HEIGHT * z);

    DIAM = 11;
    _cyl(SIZEX,SIZEY,SIZEZ,DIAM,x,y,z,CENTER,SPACING);
}

module aa(x,y,z, CENTER=false, SPACING=2) {

    SIZEX = 15;
    SIZEY = 15;
    SIZEZ = 5 + (HEIGHT * z);

    DIAM = 15;
    _cyl(SIZEX,SIZEY,SIZEZ,DIAM,x,y,z,CENTER,SPACING);
}

module _cyl(SIZEX,SIZEY,SIZEZ,DIAM,x,y,z, CENTER=false, SPACING=2) {


    STARTX = 3.5;
    SPACINGX = SPACING;
    STARTY = 3.5;
    SPACINGY = SPACING;
    STARTZ = 5;

    COUNTX=floor(((x * LENGTH) - (2*STARTX) + SPACINGX) / (SIZEX + SPACINGX));

    COUNTY=floor(((y * WIDTH) - (2*STARTY) + SPACINGY) / (SIZEY + SPACINGY));

    STEPX = SIZEX + SPACINGX;
    STEPY = SIZEY + SPACINGY;

    CENTERX = ((x * LENGTH) - (STEPX * COUNTX) + SPACINGX)/2;
    CENTERY = ((y * WIDTH) - (STEPY * COUNTY) + SPACINGY)/2;

    XSTART=CENTER ? CENTERX : STARTX;
    YSTART=CENTER ? CENTERY : STARTY;

    difference() {
        base(x,y,z);

        for(x =[XSTART:STEPX:XSTART+(COUNTX*STEPX)-0.1]) {
            for(y =[YSTART:STEPY:YSTART+(COUNTY*STEPY)-0.1]) {
                translate([x+(SIZEX/2),y+(SIZEY/2),STARTZ]) {
                    cylinder(d=DIAM, h=SIZEZ);
                }
            }
        }
    }
}