include <base.scad>;



module tray(x=1,y=1,z=1,COUNTX=1,COUNTY=1) {

    STARTX = 5;
    SPACINGX = 3;
    STARTY = 5;
    SPACINGY = 3;
    STARTZ = 5;

    SIZEX=((LENGTH * x) - ((STARTX * 2) + ((COUNTX - 1) * SPACINGX))) / (COUNTX);
    SIZEY=((WIDTH * y) - ((STARTY *  2) + ((COUNTY - 1) * SPACINGX))) / (COUNTY);
    SIZEZ=(HEIGHT * z) - STARTZ + 1;

    //STEPX = ((LENGTH * x) - (STARTX)) / (COUNTX);
    //STEPY = ((WIDTH * y) - (STARTX)) / (COUNTY);
    STEPX = SIZEX + SPACINGX;
    STEPY = SIZEY + SPACINGY;
    difference() {
        base(x,y,z);

        for(x =[STARTX:STEPX:STARTX+(COUNTX*STEPX)-0.1]) {
            for(y =[STARTY:STEPY:STARTY+(COUNTY*STEPY)-0.1]) {
                translate([x,y,STARTZ]) {
                    cube([SIZEX,SIZEY,SIZEZ]);
                }
            }
        }
    }
}