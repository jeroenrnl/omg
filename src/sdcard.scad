include <base.scad>;



module sdcard(x,y, CENTER=false, SPACING=3) {

    SIZEX = 24.5;
    SIZEY = 2.5;
    SIZEZ = 10;

    _card(SIZEX,SIZEY,SIZEZ,x,y,CENTER,SPACING);
}


module microsdcard(x,y, CENTER=false, SPACING=1.5) {

    SIZEX = 11.5;
    SIZEY = 1;
    SIZEZ = 15;

    _card(SIZEX,SIZEY,SIZEZ,x,y,CENTER,SPACING);
}

module _card(SIZEX,SIZEY,SIZEZ, x,y, CENTER=false, SPACING=1.5) {

    STARTX = 3.5;
    SPACINGX = SPACING;
    STARTY = 3.5;
    SPACINGY = SPACING;
    STARTZ = 10;

    COUNTX=floor(((x * LENGTH) - (2*STARTX) + SPACINGX) / (SIZEX + SPACINGX));

    COUNTY=floor(((y * WIDTH) - (2*STARTY) + SPACINGY) / (SIZEY + SPACINGY));

    STEPX = SIZEX + SPACINGX;
    STEPY = SIZEY + SPACINGY;

    CENTERX = ((x * LENGTH) - (STEPX * COUNTX) + SPACINGX)/2;
    CENTERY = ((y * WIDTH) - (STEPY * COUNTY) + SPACINGY)/2;

    XSTART=CENTER ? CENTERX : STARTX;
    YSTART=CENTER ? CENTERY : STARTY;

    difference() {
        base(x,y);

        for(x =[XSTART:STEPX:XSTART+(COUNTX*STEPX)-0.1]) {
            for(y =[YSTART:STEPY:YSTART+(COUNTY*STEPY)-0.1]) {
                translate([x,y,STARTZ]) {
                    cube([SIZEX,SIZEY,SIZEZ]);
                }
            }
        }
    }
}