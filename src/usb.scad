include <base.scad>;

module usb(x,y, CENTER=false, SPACING=2) {

    USBX = 5.5;
    USBY = 12.5;
    USBZ = 10;


    STARTX = 5;
    SPACINGX = SPACING;
    STARTY = 3.5;
    SPACINGY = SPACING;
    STARTZ = 5;

    COUNTX=floor(((x * LENGTH) - (2*STARTX) + SPACINGX) / (USBX + SPACINGX));

    COUNTY=floor(((y * WIDTH) - (2*STARTY) + SPACINGY) / (USBY + SPACINGY));

    STEPX = USBX + SPACINGX;
    STEPY = USBY + SPACINGY;

    CENTERX = ((x * LENGTH) - (STEPX * COUNTX) + SPACINGX)/2;
    CENTERY = ((y * WIDTH) - (STEPY * COUNTY) + SPACINGY)/2;

    XSTART=CENTER ? CENTERX : STARTX;
    YSTART=CENTER ? CENTERY : STARTY;

    difference() {
        base(x,y);

        for(x =[XSTART:STEPX:XSTART+(COUNTX*STEPX)-0.1]) {
            for(y =[YSTART:STEPY:YSTART+(COUNTY*STEPY)-0.1]) {
                translate([x,y,STARTZ]) {
                    cube([USBX, USBY, USBZ]);
                }
            }
        }
    }
}