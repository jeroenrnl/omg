/*
 * Organizer in Modular Grid (OMG)
 */
$fn = 36;

WIDTH = 25;
LENGTH = 25;
HEIGHT = 15;
ROUNDING = 0.2;

CONNECTORL = 15;
CONNECTORW = 2;
CONNECTORH = 10;
CN_ROUNDING = 0.5;
CN_MARGIN_FEM = 0.7;

module wall_f(SIZE=1) {
    WIDTH=WIDTH/5;
    SIZEZ=1;
    SIZEX=SIZE;
    SIZEY=1;

    mirror([1,0,0]) {

        rotate([0,0,90]) {
            difference() {
                wallmount(SIZE, LEFT=true, RIGHT=false);
                for(i=[1:SIZEX]) {

                    translate([LENGTH*(i-1)+(LENGTH-CONNECTORL+CN_ROUNDING-CN_MARGIN_FEM)/2,CONNECTORW-CN_ROUNDING,-CN_ROUNDING + (HEIGHT/5)]) {
                        mirror([0,1,0]) {

                            fconnector_large();
                        }
                    }
                }
            }
        }
    }
}
module wall_m(SIZE=1) {
    WIDTH=WIDTH/5;
    SIZEZ=1;
    SIZEX=SIZE;
    SIZEY=1;

    rotate([0,0,90]) {
        union() {


            wallmount(SIZE, LEFT=true, RIGHT=false);
            for(i=[1:SIZEX]) {
                translate([LENGTH*(i-1) + (LENGTH-CONNECTORL)/2,-(CONNECTORW-CN_ROUNDING),0]) {
                    mconnector();
                }
            }
        }
    }
}


module edge_f(SIZE=1, CORNERL=false, CORNERR=false, ROUNDED = false, DROUNDED = false) {
    WIDTH=WIDTH/5;
    SIZEZ=1;
    SIZEX=SIZE;
    SIZEY=1;

    difference() {
        edge(SIZE, CORNERL, CORNERR, ROUNDED, DROUNDED);
        for(i=[1:SIZEX]) {

            translate([LENGTH*(i-1)+(LENGTH-CONNECTORL+CN_ROUNDING-CN_MARGIN_FEM)/2,CONNECTORW-CN_ROUNDING,-CN_ROUNDING]) {
                mirror([0,1,0]) {

                    fconnector();
                }
            }
        }
    }
}

module edge_m(SIZE=1, CORNERL=false, CORNERR=false, ROUNDED = false, DROUNDED = false) {
    WIDTH=WIDTH/5;
    SIZEZ=1;
    SIZEX=SIZE;
    SIZEY=1;

    union() {
        edge(SIZE, CORNERL, CORNERR, ROUNDED, DROUNDED);
        for(i=[1:SIZEX]) {
            translate([LENGTH*(i-1) + (LENGTH-CONNECTORL)/2,-(CONNECTORW-CN_ROUNDING),0]) {
                mconnector();
            }
        }
    }
}

module wallmount(SIZE=1, LEFT=false, RIGHT=false) {
    WIDTH=WIDTH/5;
    SIZEZ=1.2;
    SIZEX=SIZE;
    SIZEY=1;


    EXTLENGTH = 5;
    START = 0;

    translate([ROUNDING + START, ROUNDING, -(HEIGHT/5) + ROUNDING]) {
        x = EXTLENGTH + (LENGTH*SIZEX)-(2*ROUNDING);
        y = (WIDTH*SIZEY)-(2*ROUNDING);
        z = (HEIGHT*SIZEZ)-(2*ROUNDING);

        DIAM=WIDTH-(2*ROUNDING);
        SCREWHEIGHT=z*2-(DIAM/2);


        difference() {
            minkowski() {

                union() {
                    difference() {
                        rounded_cube(x,y,z,0,0,0, false);
                        difference() {
                            translate([0,-2,0]) {
                                cube([5,WIDTH+8,5]);
                            }
                            translate([5,WIDTH+6,5]) {
                                rotate([90,0,0]) {
                                    cylinder(r=5,h=WIDTH+8);
                                }
                            }
                        }

                    }

                    translate([x-2,0,-z]) {
                        union() {
                            hull() {
                                if (LEFT) {
                                    translate([0,(WIDTH*1.5)-ROUNDING,SCREWHEIGHT]) {
                                        rotate([0,90,0]) {
                                            cylinder(d=DIAM,h=2);
                                        }
                                    }
                                }
                                if (RIGHT) {
                                    translate([0,-(WIDTH/2)-ROUNDING,SCREWHEIGHT]) {
                                        rotate([0,90,0]) {
                                            cylinder(d=DIAM,h=2);
                                        }
                                    }
                                }
                                translate([0,(WIDTH/2)-ROUNDING,SCREWHEIGHT]) {
                                    rotate([0,90,0]) {
                                        cylinder(d=DIAM,h=2);
                                    }
                                }
                            }
                            hull() {
                                translate([0,(WIDTH/2)-ROUNDING,SCREWHEIGHT]) {
                                    rotate([0,90,0]) {
                                        cylinder(d=DIAM,h=2);
                                    }
                                }

                                translate([0,(WIDTH/2)-ROUNDING,3]) {
                                    rotate([0,90,0]) {
                                        cylinder(d=DIAM,h=2);
                                    }
                                }
                            }
                        }
                        //rounded_cube(2,y,HEIGHT*3,0,0,0,false);
                    }
                    difference() {
                        /// Support under holder
                        translate([x-HEIGHT-2,0,-HEIGHT+(2*ROUNDING)]) {
                            rounded_cube(HEIGHT+2,y,HEIGHT,0,0,0,false);
                        }
                        // Round cutout for better looks and filament saving
                        translate([x-HEIGHT-2,WIDTH+6,-HEIGHT]) {
                            rotate([90,0,0]) {
                                cylinder(r=HEIGHT,h=WIDTH+8);
                            }
                        }
                        // Small cutout around screw
                        // slows down render a lot!
                        translate([x-5,WIDTH/2-ROUNDING,-(HEIGHT-(2*ROUNDING))]) {
                            rotate([0,90,0]) {
                                cylinder(d=WIDTH-0.5,h=5);
                            }
                        }

                    }
                }
                sphere(ROUNDING);
            }
            // screwholes
            translate([x-3,0,-z]) {
                translate([0,(WIDTH/2)-ROUNDING,3]) {
                    rotate([0,90,0]) {
                        cylinder(r=1,h=7);
                        cylinder(r1=2.5, r2=1, h=1.5);
                    }
                }

                translate([0,-(WIDTH/2)-ROUNDING,SCREWHEIGHT]) {
                    rotate([0,90,0]) {
                        cylinder(r=1,h=7);
                        cylinder(r1=2.5, r2=1, h=1.5);
                    }
                }
                translate([0,(WIDTH*1.5)-ROUNDING,SCREWHEIGHT]) {
                    rotate([0,90,0]) {
                        cylinder(r=1,h=7);
                        cylinder(r1=2.5, r2=1, h=1.5);
                    }
                }
            }
        }
    }
}



module edge(SIZE=1, CORNERL=false, CORNERR=false, ROUNDED = false, DROUNDED=false) {
    WIDTH=WIDTH/5;
    SIZEZ=1;
    SIZEX=SIZE;
    SIZEY=1;


    EXTLENGTH = (CORNERR ? WIDTH : 0) + (CORNERL ? WIDTH : 0);
    START = (CORNERR ? -WIDTH : 0);

    translate([ROUNDING + START, ROUNDING, ROUNDING]) {
        minkowski() {
            x = EXTLENGTH + (LENGTH*SIZEX)-(2*ROUNDING);
            y = (WIDTH*SIZEY)-(2*ROUNDING);
            z = (HEIGHT*SIZEZ)-(2*ROUNDING);
            TOP = ROUNDED ? WIDTH-(2*ROUNDING) : 0;
            LEFT = (ROUNDED && CORNERL) ? WIDTH-(2*ROUNDING) : 0;
            RIGHT = (ROUNDED && CORNERR) ? WIDTH-(2*ROUNDING) : 0;
            rounded_cube(x,y,z,LEFT,TOP,RIGHT, DROUNDED);
            sphere(ROUNDING);
        }
    }

}


module rounded_cube(x,y,z,LEFT=0,TOP=5,RIGHT=0, DROUNDED=false) {
    $fn = 72;
    union() {
        translate([RIGHT,0,z-TOP]) {
            intersection() {
                cube([x-LEFT-RIGHT,y,TOP]);
                rotate([0,90,0]) {
                    cylinder(h=x, r=TOP);
                }
            }

        }
        // LEFT ROUNDING
        translate([x-LEFT,0,0]) {
            intersection() {
                cube([LEFT,y,z]);
                cylinder(h=z-TOP, r=LEFT);
            }
        }
        // RIGHT ROUNDING
        translate([0,0,0]) {
            intersection() {
                cube([RIGHT,y,z]);
                translate([RIGHT,0,0]) {
                    cylinder(h=z-TOP, r=RIGHT);
                }
            }
        }
        // LEFT top corner
        translate([0,0,0]) {
            intersection() {
                translate([0,0,z-TOP]) {
                    intersection() {
                        cube([x,y,TOP]);
                        rotate([0,90,0]) {
                            cylinder(h=x, r=TOP);
                        }
                        translate([x-LEFT,0,0]) {
                            cylinder(h=z-TOP, r=LEFT);
                        }
                        if(DROUNDED) {
                            translate([x-LEFT,0,0]) {
                                translate([0,LEFT,0]) {
                                    rotate([90,0,0]) {
                                        cylinder(h=x, r=LEFT);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        // RIGHT top corner
        translate([-RIGHT,0,0]) {
            intersection() {
                translate([RIGHT,0,z-TOP]) {
                    intersection() {
                        cube([x,y,TOP]);
                        rotate([0,90,0]) {
                            cylinder(h=x, r=TOP);
                        }
                        translate([RIGHT,0,0]) {
                            cylinder(h=z-TOP, r=RIGHT);
                        }
                        if(DROUNDED) {
                            translate([RIGHT,RIGHT,0]) {

                                rotate([90,0,00]) {
                                    cylinder(h=x, r=TOP);
                                }
                            }
                        }
                    }
                }
            }


        }
        translate([RIGHT,0,0]) {
            cube([x - LEFT - RIGHT,y,z-TOP]);
        }
    }

}

module base(SIZEX=1, SIZEY=1, SIZEZ=1) {
     difference() {
        union() {
            translate([ROUNDING, ROUNDING, ROUNDING]) {
                minkowski() {
                    cube([(LENGTH*SIZEX)-(2*ROUNDING), (WIDTH*SIZEY)-(2*ROUNDING),(HEIGHT*SIZEZ)-(2*ROUNDING)]);
                    sphere(ROUNDING);
                }
            }
            for(i=[1:SIZEX]) {

                translate([LENGTH*(i-1) + (LENGTH-CONNECTORL)/2,(WIDTH*SIZEY)+CONNECTORW-CN_ROUNDING,0]) {
                    mirror([0,1,0]) {
                        mconnector();
                    }

                }
            }
            for(i=[1:SIZEY]) {
                translate([(LENGTH*SIZEX)+CONNECTORW-CN_ROUNDING,WIDTH*(i-1)+(WIDTH-CONNECTORL)/2,0]) {
                    rotate([0,0,90]) {
                        mconnector();
                    }
                }
            }
        }
        for(i=[1:SIZEX]) {

            translate([LENGTH*(i-1)+(LENGTH-CONNECTORL+CN_ROUNDING-CN_MARGIN_FEM)/2,CONNECTORW-CN_ROUNDING,-CN_ROUNDING]) {
                mirror([0,1,0]) {

                    fconnector();
                }
            }
        }
        for(i=[1:SIZEY]) {

            translate([CONNECTORW-CN_ROUNDING,WIDTH*(i-1)+(WIDTH-CONNECTORL+CN_ROUNDING-CN_MARGIN_FEM)/2,-CN_ROUNDING]) {
                rotate([0,0,90]) {
                    fconnector();
                }
            }
        }
    }

}

module fconnector_large() {
    L = CONNECTORL;
    W = CONNECTORW;
    H = HEIGHT;
    R = CN_ROUNDING + CN_MARGIN_FEM;
    connector(L,W,H,R);
}

module fconnector() {
    L = CONNECTORL;
    W = CONNECTORW;
    H = CONNECTORH;
    R = CN_ROUNDING + CN_MARGIN_FEM;
    connector(L,W,H,R);
}
module mconnector() {
    L = CONNECTORL;
    W = CONNECTORW;
    H = CONNECTORH;
    R = CN_ROUNDING;
    connector(L,W,H,R);

}

module connector(L,W,H,R) {
    CW = W - (R/2);
    CL = L - (R/2);
    CH = H - (R/2);
    translate([0,0,R]) {
        minkowski() {

            linear_extrude(CH) {
                polygon([[0,0], [CW,CW], [CL-CW,CW], [CL,0]]);
            }
            sphere(R);
        }
    }
}


