Organizer in Modular Grid
=========================
![demo](demo/demo.png "rendering of a complete desk tray")

This is a set of highly customizable, expandable and modular trays to create your own desk tray or wall-mounted tray. Each module is purpose-built to store a specific item, but by mixing and matching, one can create a personal combination of the required storage. If this turns out to be insufficent in the future, items can be easily replaced and reused.

The grid is a 25mm x 25mm grid, where each module can occupy 1 or more grid squares in each direction. The height of each unit is 15mm and some modules can be stretched in this direction as well.

Currently, the following storage options are available:
- SD Cards
- Micro SD Cards
- USB sticks
- Batteries (AA and AAA)
- Screw bits (hex)
- Generic trays

Furthermore, borders can be added for a finished look.

If you have any ideas for addition, please create an issue or fork this project, create your own addition and create a merge request. *Provide measurements in mm only*.

All modules have been designed in OpenSCAD and this program can be used to create your own customized modules. 

For those who are uncomfortable use this program, ready made STL files are provided for some common sizes. These are also published on https://prusaprinters.org/. In the near future, I will publish customizable OpenSCAD files, so 

This is all licensed under the Creative Commons, Attribution, Non Commercial, Share Alike license.
https://creativecommons.org/licenses/by-nc-sa/4.0/

This means that you can use, copy, share and modify this, as long as you do not change the license, give me credits and don't ask money for it.
(If you want to use it in your office, that is fine)
